<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--Boostrap-->
  <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.css">

  <!--CSS-->
  <link rel="stylesheet" href="/assets/css/navbar.css">
  <link rel="stylesheet" href="/assets/css/button.css">
  <link rel="stylesheet" href="/assets/css/paragraph.css">

  <title>Veilles</title>
</head>

<body style="background-image: url(/assets/wp2088351.jpg);">

  <!--Navbar-->
<div class="container">
  <div class="row">
    <div class="col-12">
  <nav class="navbar" style="background: linear-gradient(#101d3a,#000000); box-shadow: 10px 5px 5px black;">

    <ul>

      <li class="p2"><a href="/accueil.html">Accueil</a></li>


      <li class="p2"><a href="/veilles.html">Veilles</a></li>


      <li class="p2"><a href="/restikos.html">Restikos</a></li>

      <li style="float:right"><a  href="/accueil.html"><img src="/assets/png-transparent-airtable-database-spreadsheet-logo-application-software-slack-logo-angle-rectangle-orange.png" width="30" height="30" alt=""></a></li>

    </ul>

  </nav>
</div>
</div>
</div>
  <!--Contenus-->

  <main>
    <div class="container">

      <div class="row">

        <div class="col-12">

          <div class="button">
            <a href="/editVeilles.html">Créer veille</a>
          </div>


        </div>

      </div>

      <div id="addVeille">

        <div class="row">

          <div class="col-12">



            <div
              style="background: linear-gradient(#3A528A, #15213B); height: 400px; width: 1000px;border-radius: 1em; box-shadow: 10px 5px 5px black;">

              <div
                style="background: url(/assets/img/codage.jpg); height: 150px; width: 250px; border-radius: 1em;">
              </div>
              <div>

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    </div>
  </main>


</body>

</html>