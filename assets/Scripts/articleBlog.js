// Base de donnée (Nom de la table)


// Fonction qui permet de vérifier si l'article est terminé. (Airtable Public)
function checkPublicArticlePublished() {
    base('public').select({
        view: "Grid view"
    }).eachPage(function page(records, fetchNextPage) {
    
        records.forEach(function(record) {
            if (record.get('status') == "published") {
                document.getElementById("addArticleBlog").innerHTML = document.getElementById("addArticleBlog").innerHTML + '<div class="row mb-5" onclick="openArticle(\'' + record.id + '\')"><div class="col"><div style="height: auto; background: url(' + record.get('image')[0].url + ') center / cover;" class="rounded"><div class="row"><div class="col" style="margin-top: 400px"><div class="p-3 rounded" style="height: auto; background-color: rgba(219, 2, 21, 0.65); margin: 20px;"><h3 class="pointer textWhite"><span>' + record.get('title') + '</span></h3><p class="textWhite"><span>' + record.get('intro') + '</span></p></div></div></div></div></div></div>';
                // console.log(record);
            }
        });
    
        fetchNextPage();
    
    }, function done(err) {
        if (err) { console.error(err); return; }
    });
}

// Fonction qui permet de vérifier si l'article est terminé. (Airtable Privé)
function checkPrivateArticlePublished() {
    base('private').select({
        view: "Grid view"
    }).eachPage(function page(records, fetchNextPage) {
    
        records.forEach(function(record) {
            if (record.get('status') == "published") {
                document.getElementById("addArticleBlog").innerHTML = document.getElementById("addArticleBlog").innerHTML + '<div class="row mt-5"><div class="col"><div onclick="getIdBlog" style="height: auto; background: url(' + record.get('image')[0].url + ') center / cover; border-radius: 20px;"><div class="row"><div class="col" style="margin-top: 400px"><div class="p-3" style="height: auto; background-color: rgba(219, 2, 21, 0.65); border-radius: 20px;"><h3 class="textWhite"><span>' + record.get('title') + '</span></h3><p class="textWhite"><span>' + record.get('intro') + '</span></p></div></div></div></div></div></div>';
                console.log(record);
            }
        });
    
        fetchNextPage();
    
    }, function done(err) {
        if (err) { console.error(err); return; }
    });
}

// Fonction qui permet de faire afficher le contenu d'un article choisis sur la page HTML Article
function putSelectArticle(idBlogClick) {
    base('author').find(idBlogClick, function(err, record) {
        if (err) {
            console.error(err); 
            return; 
        }
        document.getElementById("authorArticle").innerHTML = '<div style="background: url(' + record.get('image')[0].url + ') center / cover; height: 100px; width: 100px; border-radius: 50%;"></div>';
        document.getElementById("profileAuthor").innerHTML = '<h2 class="titreArticle"><span>' + record.get('person') + '</span></h2><h3 class="introductionArticle m-0 p-0 pl-5"><span>' + record.get("position") + '</span></h3><p>Date : <span id="dateArticle"></span></p>';
    });
    base('public').find(idBlogClick, function(err, record) {
        if (err) {
            console.error(err); 
            return; 
        }
        document.getElementById("titreArticle").innerHTML = '<span>' + record.get('title') + '</span>';
        document.getElementById("introductionArticle").innerHTML = '<span>' + record.get('intro') + '</span>';
    });
    base('private').find(idBlogClick, function(err, record) {
        if (err) {
            console.error(err); 
            return; 
        }
        document.getElementById("titreArticle").innerHTML = '<span>' + record.get('title') + '</span>';
        document.getElementById("introductionArticle").innerHTML = '<span>' + record.get('intro') + '</span>';
        document.getElementById("dateArticle").innerHTML = record.get('date');
    });
}

// Fonction qui renvoie à un article clické
function openArticle(article_id) {
    location.href = "articleBlog.html?id=" + article_id;
}

// Fonction qui affiche du text dans le code html
function printArticle() {
    var myurl = new URL(document.location.href);
    var search_params = new URLSearchParams(myurl.search); 
    if (search_params.has('id')) {
        base('public').find(search_params.get('id'), function(err, record) {
            if (err) { console.error(err); return; }
            document.getElementById('addImageArticle').style.background = ('url(' + record.get('image')[0].url + ') center / cover')
            document.getElementById('titreArticle').innerHTML = record.get('title');
            document.getElementById('introductionArticle').innerHTML = record.get('intro');
            document.getElementById('dateArticle').innerHTML = record.get('date');
            document.getElementById('addContent').innerHTML = record.get('content');
            base('authors').find(record.get('author_id'), function(err, record) {
                if (err) { console.error(err); return; }
                document.getElementById('putNameAuthor').innerHTML = record.get('person').name;
                document.getElementById('putPost').innerHTML = record.get('position');
                document.getElementById('authorArticle').innerHTML = '<div style="background: url(' + record.get('profile_picture')[0].url + ') center / cover; height: 100px; width: 100px; border-radius: 50%;"></div>';
            });
        });
    }
}

// Fonction qui ajoute les 3 derniers articles sur la page article blog (partie gauche)
// Fait par : Tehau
// Modifié par : Tehau
function printVeilleReduct() {
    base('public').select({
        maxRecords: 3,
        view: "Grid view"
    }).eachPage(function page(records, fetchNextPage) {
    console.log("records", records);
        records.forEach(function(record, index) {
            document.getElementById('addNewVeille').innerHTML = document.getElementById('addNewVeille').innerHTML + '<div class="row mb-3" onclick="openArticle(\'' + record.id + '\')"><div class="col-12"><div class="float-left mr-3 rounded" style="background: url(' + record.get('image')[0].url + ') no-repeat center / cover; width: 100px; height: 100px;"></div><h6 style="font-weight: bold;">' + record.get('title') + '</h6><p style="line-height: 15px;">' + record.get('intro') + '</p></div></div>';
        });
        
        fetchNextPage();
    
    }, function done(err) {
        if (err) { console.error(err); return; }
    });
}